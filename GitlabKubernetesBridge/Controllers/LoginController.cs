﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GitlabKubernetesBridge.Controllers
{
    public class LoginController : ControllerBase
    {

        public LoginController()
        {
        }

        /// <summary>
        /// Test endpoint for angular guard, if user is authenticated.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/test")]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task test()
        {
            this.Response.StatusCode = 200;
        }

        [HttpGet]
        [Route("/api/v1/kubernetes/oauth/auth")]
        public virtual IActionResult DashBoardOAuthUrl()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                var token = Org.OpenAPITools.Controllers.GitlabApiController.GetUserServiceAccount(this.User.Identity.Name, true);
                this.Response.Headers.Add("Authorization", $"Bearer {token}");
                return StatusCode(202);
            }
            else
            {
                return StatusCode(401);
            }
        }

    }
}