﻿using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace GitlabKubernetesBridge.Server.Helper
{
    public class GitlabApiClient
    {
        private HttpClient client;

        public GitlabApiClient(string token)
        {
            this.client = new HttpClient();
            this.client.BaseAddress = new Uri(Environment.GetEnvironmentVariable("GITLAB_URL") + "api/v4/");
            this.client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }

        public T MakeGet<T>(string url, Dictionary<string, string> param)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("?");
            foreach (var item in param)
            {
                builder.Append(item.Key + "=" + System.Uri.EscapeDataString(item.Value) + "&");
            }


            var res = this.client.GetAsync(url + builder.ToString()).Result;

            if (!res.IsSuccessStatusCode)
            {
                throw new Exception("Unexpectet return code: " + res.StatusCode + "; Msg: " + res.Content.ReadAsStringAsync().Result);
            }

            return JsonSerializer.Deserialize<T>(res.Content.ReadAsStringAsync().Result);
        }

        public T MakePost<T>(string url, object data)
        {
            var res = this.client.PostAsJsonAsync(url, data).Result;

            if (!res.IsSuccessStatusCode)
            {
                throw new Exception("Unexpectet return code: " + res.StatusCode + "; Msg: " + res.Content.ReadAsStringAsync().Result);
            }

            string ret = res.Content.ReadAsStringAsync().Result;

            return JsonSerializer.Deserialize<T>(ret);
        }

        public T MakePut<T>(string url, object data)
        {
            var res = this.client.PutAsJsonAsync(url, data).Result;

            if (!res.IsSuccessStatusCode)
            {
                throw new Exception("Unexpectet return code: " + res.StatusCode + "; Msg: " + res.Content.ReadAsStringAsync().Result);
            }

            string ret = res.Content.ReadAsStringAsync().Result;

            return JsonSerializer.Deserialize<T>(ret);
        }

        public void MakeDelete<T>(string url)
        {
            var res = this.client.DeleteAsync(url).Result;

            if (!res.IsSuccessStatusCode)
            {
                throw new Exception("Unexpected return code: " + res.StatusCode + "; Msg: " + res.Content.ReadAsStringAsync().Result);
            }
        }
    }
}
