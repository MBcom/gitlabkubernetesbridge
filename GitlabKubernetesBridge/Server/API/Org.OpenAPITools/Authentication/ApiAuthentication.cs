using Microsoft.AspNetCore.Authorization;

namespace Org.OpenAPITools.Authentication
{
    public class ApiKeyRequirement : IAuthorizationRequirement
    {
        public IReadOnlyList<string> ApiKeys { get; set; }

        public string PolicyName { get; set; }

        public ApiKeyRequirement(IEnumerable<string> apiKeys, string policyName)
        {
            this.ApiKeys = apiKeys?.ToList() ?? new List<string>();
            this.PolicyName = policyName;
        }
    }

    public class ApiKeyRequirementHandler : AuthorizationHandler<ApiKeyRequirement>
    {

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, ApiKeyRequirement requirement)
        {
            SucceedRequirementIfApiKeyPresentAndValid(context, requirement);
            return Task.CompletedTask;
        }

        private void SucceedRequirementIfApiKeyPresentAndValid(AuthorizationHandlerContext context, ApiKeyRequirement requirement)
        {

        }
    }
}

