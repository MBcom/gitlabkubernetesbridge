/*
 * API description for gitlab-kubernetes-bridge
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 */

using GitlabKubernetesBridge.Server.Models.GitlabAPI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Org.OpenAPITools.Attributes;
using Swashbuckle.AspNetCore.Annotations;

namespace Org.OpenAPITools.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [ApiController]
    public class KubernetesApiController : ControllerBase
    {
        private readonly ILogger<KubernetesApiController> _logger;

        public KubernetesApiController(ILogger<KubernetesApiController> logger)
        {
            this._logger = logger;
        }

        /// <summary>
        /// Returns a config file ready to save in ~/.kube/config
        /// </summary>
        /// <response code="200">successfull</response>
        [HttpGet]
        [Route("/api/v1/kubernetes/getUserConf")]
        [ValidateModelState]
        [SwaggerOperation("GetUserConf")]
        [SwaggerResponse(statusCode: 200, type: typeof(string), description: "successfull")]
        public virtual IActionResult GetUserConf()
        {
            this._logger.LogInformation($"Get kubectl user config for user {this.User.Identity.Name}");

            System.Net.Mime.ContentDisposition cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "config",
                Inline = false  // false = prompt the user for downloading;  true = browser to try to show the file inline
            };
            this.Response.Headers.Add("Content-Disposition", cd.ToString());
            this.Response.Headers.Add("X-Content-Type-Options", "nosniff");


            var token = Org.OpenAPITools.Controllers.GitlabApiController.GetUserServiceAccount(this.User.Identity.Name, true);

            return File(System.Text.Encoding.UTF8.GetBytes(new KubenertesUserConfig(this.User.Identity.Name + "-account", token).ToString()), "text/plain");
        }

        /// <summary>
        /// Returns a token for the user, ready to login to kubernetes dashboard e.g.
        /// </summary>
        /// <response code="200">successfull</response>
        [HttpPost]
        [Route("/api/v1/kubernetes/getUserToken")]
        [ValidateModelState]
        [SwaggerOperation("GetUserToken")]
        [SwaggerResponse(statusCode: 200, type: typeof(string), description: "successfull")]
        public virtual IActionResult GetUserToken()
        {
            var token = Org.OpenAPITools.Controllers.GitlabApiController.GetUserServiceAccount(this.User.Identity.Name, true);

            this.Response.ContentType = "text/plain";

            return Ok(token);
        }
    }
}
