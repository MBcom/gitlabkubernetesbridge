using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace GitlabKubernetesBridge.Server.Models
{
    /// <summary>
    /// Model for Kubernetes Resource Limits or Requests
    /// </summary>
    public partial class KubernetesResourcesConfiguration
    {
        /// <summary>
        /// Gets or Sets CPU value
        /// </summary>
        [JsonPropertyName("cpu")]
        public string CPU { get; set; }

        /// <summary>
        /// Gets or Sets RAM value
        /// </summary>
        [JsonPropertyName("memory")]
        public string Memory { get; set; }
    }
}
