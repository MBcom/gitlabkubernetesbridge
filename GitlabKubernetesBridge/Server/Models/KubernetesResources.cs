using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace GitlabKubernetesBridge.Server.Models
{
    /// <summary>
    /// Model for Kubernetes Resource Limits or Requests
    /// </summary>
    public partial class KubernetesResources
    {
        /// <summary>
        /// Gets or Sets the Gitlab Kubernetes Agent resource limits
        /// </summary>
        [JsonPropertyName("limits")]
        public KubernetesResourcesConfiguration? Limits { get; set; }

        /// <summary>
        /// Gets or Sets the Gitlab Kubernetes Agent resource requests
        /// </summary>
        [JsonPropertyName("requests")]
        public KubernetesResourcesConfiguration? Requests { get; set; }
    }
}
