using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace GitlabKubernetesBridge.Server.Models
{
    /// <summary>
    /// 
    /// </summary>
    public partial class AgentConfiguration
    {
        /// <summary>
        /// Gets or Sets the number of Gitlab Kubernetes Agent replicas
        /// </summary>
        [JsonPropertyName("replicas")]
        public int? Replicas { get; set; }

        /// <summary>
        /// Gets or Sets the Gitlab Kubernetes Agent resource limits
        /// </summary>
        [JsonPropertyName("resources")]
        public KubernetesResources Resources { get; set; }
    }
}
