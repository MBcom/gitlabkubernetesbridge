﻿namespace GitlabKubernetesBridge.Server.Models
{
    public class AppConfig
    {
        /// <summary>
        /// GitLab Kubernetes Agent Server Endpoint address
        /// </summary>
        public string GitlabKubernetesAgentServer { get; set; } = Environment.GetEnvironmentVariable("GITLAB_KUBERNETES_AGENT_ENDPOINT") ?? "";

        /// <summary>
        /// Name of the GitLab CI variable which will contain the created namespace name. 
        /// </summary>
        public string GitlabKubeNamespaceVariableName { get; set; } = Environment.GetEnvironmentVariable("GITLAB_KUBE_NAMESPACE_VARIABLE_NAME") ?? "KUBE_NAMESPACE";

        /// <summary>
        /// Name of the GitLab CI variable which will contain the required kubectl context name to use the created bridge. 
        /// </summary>
        public string GitlabKubeContextVariableName { get; set; } = Environment.GetEnvironmentVariable("GITLAB_KUBE_CONTEXT_VARIABLE_NAME") ?? "KUBE_CONTEXT";

        /// <summary>
        /// Name of the Gitlab Kubernetes Agent that will be created by this GKB in a project on bridge creation.
        /// </summary>
        public string GitlabKubeContextName { get; set; } = Environment.GetEnvironmentVariable("GITLAB_KUBE_CONTEXT_NAME") ?? "gkb-agent";

        /// <summary>
        /// Kubernetes label name which will be set to the Kubernetes namespaces.
        /// The label value will be the Gitlab's project id for which a bridge will be created.
        /// </summary>
        public string NamespaceProjectLabelName { get; set; } = Environment.GetEnvironmentVariable("NAMESPACE_PROJECT_LABEL_NAME") ?? "mlu-gitlab-project-id";

        /// <summary>
        /// Name of the GitLab CI variable which will the configured base domain for this cluster and GKB. 
        /// </summary>
        public string KubeIngressBasePathVariableName { get; set; } = Environment.GetEnvironmentVariable("KUBE_INGRESS_BASE_PATH_VARIABLE_NAME") ?? "KUBE_INGRESS_BASE_DOMAIN";

        public string KubeIngressBasePath { get; set; } = Environment.GetEnvironmentVariable("KUBE_INGRESS_BASE_PATH") ?? "not-set";

        /// <summary>
        /// Optional admin token for GitLab API
        /// </summary>
        public string GitlabManagementToken { get; set; } = Environment.GetEnvironmentVariable("GITLAB_MANAGE_TOKEN") ?? "";

        /// <summary>
        /// Gitlab Base URL
        /// </summary>
        public string GitlabBaseURL { get; set; } = Environment.GetEnvironmentVariable("GITLAB_URL") ?? "not-set";

        /// <summary>
        /// Path to helm executable
        /// </summary>
        public string HelmPath { get; set; } = Environment.GetEnvironmentVariable("HELM_PATH") ?? "/usr/local/bin/helm";

        /// <summary>
        /// GitLab's ID to a GitLab runner which will be added/ allowed to project on bridge creation.
        /// </summary>
        public string? SpecialRunnerID { get; set; } = Environment.GetEnvironmentVariable("SPECIAL_RUNNER_ID");

        /// <summary>
        /// A custom postfix which will be attached to generated namespace names
        /// </summary>
        public string KubeNamespaceNamePostfix { get; set; } = Environment.GetEnvironmentVariable("KUBE_NAMESPACE_NAME_POSTFIX") ?? "";

        /// <summary>
        /// Optional specific version to install specific version.
        /// </summary>
        public string? HelmGitlabKubernetesAgentChartVersion { get; set; } = Environment.GetEnvironmentVariable("HELM_GITLAB_KUBERNETES_AGENT_CHART_VERSION");

        public string CustomNamespaceProjectOwnerLabelName { get; set; } = Environment.GetEnvironmentVariable("NAMESPACE_PROJECT_OWNER_LABEL_NAME") ?? "mlu-custom-project-owner";

        public string CustomNamespaceProjectMemberLabelName { get; set; } = Environment.GetEnvironmentVariable("NAMESPACE_PROJECT_MEMBER_LABEL_NAME") ?? "mlu-custom-project-member";

        public string WildcardCertificateKey { get; set; } = Environment.GetEnvironmentVariable("KUBECERT_KEY") ?? "";

        public string WildcardCertificateCert { get; set; } = Environment.GetEnvironmentVariable("KUBECERT_CRT") ?? "";
     }
}
