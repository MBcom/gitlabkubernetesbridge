import { Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SecurityGuard } from './guards/security.guard';
import { NoKubeDashboardComponent } from './components/no-kube-dashboard/no-kube-dashboard.component';

export const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "", component: DashboardComponent, canActivate: [SecurityGuard] },
  { path: "dashboard", component: NoKubeDashboardComponent }
];
