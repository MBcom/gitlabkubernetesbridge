import { Component, afterNextRender } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  window = window;
  isNavbarCollapsed = true;
  brandString: string = "";

  constructor(public router: Router) {
    afterNextRender(() => {
      // This will only run in the browser, not the server.
      fetch("/branding/brand.txt", {
        method: "GET"
      }).then(r => {
        if (r.ok) {
          r.text().then(t => this.brandString = t);
        }
      });
    });
  }

}
