import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoKubeDashboardComponent } from './no-kube-dashboard.component';

describe('NoKubeDashboardComponent', () => {
  let component: NoKubeDashboardComponent;
  let fixture: ComponentFixture<NoKubeDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoKubeDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoKubeDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
