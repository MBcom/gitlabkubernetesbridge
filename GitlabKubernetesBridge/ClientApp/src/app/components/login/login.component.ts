import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private route: ActivatedRoute){

  }

  ngOnInit() {
  }

  login(): void{
    var url : string = "/api/v1/account/login";
    if (this.route.snapshot.queryParams.redirect){
      url += "?redirect="+this.route.snapshot.queryParams.redirect;
    }
    window.location.href = url;
  }

  isFailed(): boolean{
    if (this.route.snapshot.queryParams.error){
      return true;
    }
    return false;
  }
}
