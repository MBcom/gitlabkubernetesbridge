import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

export const providers = [
  
];

if (environment.production) {
  enableProdMode();
}

window.document.addEventListener('DOMContentLoaded', () => {
     platformBrowserDynamic(providers).bootstrapModule(AppModule)
  .catch(err => console.log(err));
   });
